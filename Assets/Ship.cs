﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	Transform myTransform;

	Camera cam;

	public float speed = 1;

	public float leftMax;
	public float rightMax;


	// Use this for initialization
	void Start () {
		myTransform = gameObject.transform;


	}
	
	// Update is called once per frame
	void Update () {



		float sWidth = Screen.width;
		float sHeight = Screen.height;

		if (myTransform.position.x < leftMax) {
			Vector3 bumpRight = new Vector3(.01f, 0,0);
			myTransform.position += bumpRight;
			return;
		}
		if (myTransform.position.x > rightMax) {
			Vector3 bumpLeft = new Vector3(-.01f, 0,0);
			myTransform.position += bumpLeft;
			return;
		}

		if (Input.GetKey(KeyCode.LeftArrow)) {
			//print ("Left button ");
			myTransform.position = new Vector3 (myTransform.position.x + (-speed * Time.deltaTime), myTransform.position.y, myTransform.position.z); // += speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			myTransform.position = new Vector3 (myTransform.position.x + (speed * Time.deltaTime), myTransform.position.y, myTransform.position.z); // += speed * Time.deltaTime;
		}
//		if (Input.GetKey(KeyCode.UpArrow)) {
//			myTransform.position = new Vector3 (myTransform.position.x , myTransform.position.y+ (speed * Time.deltaTime), myTransform.position.z); // += speed * Time.deltaTime;
//		}
//		if (Input.GetKey(KeyCode.DownArrow)) {
//			myTransform.position = new Vector3 (myTransform.position.x, myTransform.position.y + (-speed * Time.deltaTime), myTransform.position.z); // += speed * Time.deltaTime;
//		}
	}
}
